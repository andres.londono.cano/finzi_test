
String urlImages = "assets/images";
String urlGif = "assets/images/gif";
String urlIcons = "assets/images/icons";
class AssetWidget {
  //ICONS
  String icon = "$urlIcons/icon.svg";
  String back = "$urlIcons/back.svg";
  String home = "$urlIcons/house.svg";
  String person = "$urlIcons/person.svg";
  String group = "$urlIcons/Group.svg";
  String box = "$urlIcons/box.svg";
  String check = "$urlIcons/check.svg";

  //IMAGES

  String background = "$urlImages/background.png";
}
