import 'dart:async';


import 'package:finzi/data/networking/endPointApi.dart';
import 'package:finzi/redux/app/app_state.dart';



import 'package:redux/redux.dart';


class SignUpMiddleware extends MiddlewareClass<AppState> {
  SignUpMiddleware(this.api);

  final endPointApi api;

  @override
  Future<void> call(
      Store<AppState> store, dynamic action, NextDispatcher next) async {
    next(action);

  }



}
