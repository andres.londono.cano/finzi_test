

import 'package:finzi/redux/sign_up/sign_up_actions.dart';
import 'package:finzi/redux/sign_up/sign_up_state.dart';

postsReducer(PostsStateSignUp prevState, SetPostsStateActionSignUp action) {
  final payload = action.postsState;
  return prevState.copyWith(
    state: payload.state,

  );
}
