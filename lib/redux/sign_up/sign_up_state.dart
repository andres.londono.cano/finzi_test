import 'package:meta/meta.dart';


@immutable
class PostsStateSignUp {
  final int state;

  PostsStateSignUp({
    this.state,

    //this.posts,
  });

  factory PostsStateSignUp.initial() => PostsStateSignUp(
        state: 0,
      );

  PostsStateSignUp copyWith({
    @required int state,

  }) {
    return PostsStateSignUp(
      state: state ?? this.state,

    );
  }
}
