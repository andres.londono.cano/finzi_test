import 'package:finzi/redux/sign_up/sign_up_state.dart';
import 'package:flutter/material.dart';



@immutable
class SetPostsStateActionSignUp {
  final PostsStateSignUp postsState;

  SetPostsStateActionSignUp(this.postsState);
}
