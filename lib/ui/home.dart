import 'package:finzi/app/app_colors.dart';
import 'package:finzi/assets/assets.dart';
import 'package:finzi/generated/l10n.dart';
import 'package:finzi/redux/sign_up/sign_up_actions.dart';
import 'package:finzi/redux/sign_up/sign_up_state.dart';
import 'package:finzi/redux/sign_up/store.dart';
import 'package:finzi/styles/style.dart';
import 'package:finzi/utils/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:redux/redux.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int state = 0;

  refresh() {
    setState(() {});
  }

  Store<AppStateSignUp> _store;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppColors.backgroundColor,
        body: StoreProvider<AppStateSignUp>(
            store: ReduxSignUp.store,
            child: StoreConnector<AppStateSignUp, dynamic>(
                //distinct: true,
                converter: (store) => ReduxSignUp.store,
                onInit: (store) {
                  _store = store;
                },
                builder: (context, value) {
                  return Stack(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 20, right: 20, top: 20),
                        child: Column(
                          children: [
                            SizedBox(
                              height: 20,
                            ),
                            SvgPicture.asset(AssetWidget().icon),
                            SizedBox(
                              height: 20,
                            ),
                            Stack(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 60),
                                  width: double.infinity,
                                  height: 450,
                                  //        height: double.infinity,
                                  decoration: BoxDecoration(
                                    //  color: Colors.transparent,
                                    gradient: LinearGradient(colors: [
                                      Color(0xFFC4C6DD),
                                      Color(0xFFE8E9FB)
                                    ]),
                                    borderRadius: BorderRadius.circular(30.0),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.4),
                                        spreadRadius: 2,
                                        blurRadius: 3,
                                        offset: Offset(
                                            0, 3), // changes position of shadow
                                      ),
                                    ],
                                  ),
                                ),
                                Column(
                                  children: [
                                    Container(
                                      //  margin: EdgeInsets.only(bottom: 100),
                                      child: Row(
                                        children: [
                                          Expanded(child: SizedBox()),
                                          buttonHome(AssetWidget().back,
                                              onTap: () {
                                                if (ReduxSignUp.store.state
                                                    .postsState.state != 0) {
                                                  changeState(
                                                      ReduxSignUp.store.state
                                                          .postsState.state -
                                                          1);
                                                }
                                              }),
                                          Expanded(child: SizedBox()),
                                          Stack(
                                            children: [
                                              Image.asset(
                                                AssetWidget().background,
                                                // color: Colors.green,
                                              ),
                                              Positioned.fill(
                                                child: Align(
                                                    alignment: Alignment.center,
                                                    child: SvgPicture.asset(
                                                        AssetWidget().group)),
                                              ),
                                            ],
                                          ),
                                          Expanded(child: SizedBox()),
                                          buttonHome(AssetWidget().home,
                                              onTap: () {
                                                changeState(0);
                                              }),
                                          Expanded(child: SizedBox())
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    ReduxSignUp.store.state.postsState.state ==
                                        0
                                        ? tab0()
                                        : SizedBox(),
                                    ReduxSignUp.store.state.postsState.state ==
                                        1
                                        ? tab1()
                                        : SizedBox(),
                                    ReduxSignUp.store.state.postsState.state ==
                                        2
                                        ? tab2()
                                        : SizedBox(),
                                    ReduxSignUp.store.state.postsState.state ==
                                        3
                                        ? tab3()
                                        : SizedBox(),
                                    ReduxSignUp.store.state.postsState.state ==
                                        4
                                        ? tab4()
                                        : SizedBox(),
                                    ReduxSignUp.store.state.postsState.state ==
                                        5
                                        ? tab5()
                                        : SizedBox(),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Positioned.fill(
                        child: Align(
                            alignment: Alignment.bottomLeft,
                            child: SvgPicture.asset(AssetWidget().person)),
                      ),
                    ],
                  );
                })));
  }

  Widget tab0() {
    return Column(
      children: [
        Text(
          AppLocalizations
              .of(context)
              .whoareyou,
          style: styleText(33, AppColors.primaryColor, false),
        ),
        SizedBox(
          height: 40,
        ),
        Row(
          children: [
            SizedBox(
              width: 25,
            ),
            // Expanded(child: SizedBox()),
            Expanded(
              flex: 5,
              child: button(AppLocalizations
                  .of(context)
                  .iama, AppLocalizations
                  .of(context)
                  .young, onTap: () {
                changeState(1);
              }),
            ),
            SizedBox(
              width: 20,
            ),
            Expanded(
              flex: 5,
              child: button(AppLocalizations
                  .of(context)
                  .iama, AppLocalizations
                  .of(context)
                  .dad, onTap: () {
                changeState(1);
              }),
            ),

            SizedBox(
              width: 25,
            ),
            //  Expanded(child: SizedBox()),
          ],
        ),
        SizedBox(
          height: 40,
        ),
      ],
    );
  }

  Widget tab1() {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Column(
        children: [

          Text(
            AppLocalizations
                .of(context)
                .whatisyourname,
            style: styleText(35, AppColors.primaryColor, false),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            AppLocalizations
                .of(context)
                .ingressname,
            style: styleText(18, AppColors.primaryColor, false),
          ),
          SizedBox(
            height: 10,
          ),
          TextField(
            autofocus: false,
            style: TextStyle(fontSize: 22.0, color: Color(0xFFbdc6cf)),
            decoration: decorationTexfield(AppLocalizations
                .of(context)
                .name),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            AppLocalizations
                .of(context)
                .ingresslastname,
            style: styleText(18, AppColors.primaryColor, false),
          ),
          SizedBox(
            height: 10,
          ),
          TextField(
            autofocus: false,
            style: TextStyle(fontSize: 22.0, color: Color(0xFFbdc6cf)),
            decoration: decorationTexfield(AppLocalizations
                .of(context)
                .apellido),
          ),
          SizedBox(
            height: 10,
          ),
          buttonSkip(AppLocalizations
              .of(context)
              .following, onTap: () {
            changeState(2);
          }),

        ],
      ),
    );
  }


  Widget tab2() {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Column(
        children: [

          Text(
            AppLocalizations
                .of(context)
                .whatisyouremail,
            style: styleText(35, AppColors.primaryColor, false),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            AppLocalizations
                .of(context)
                .ingressemail,
            style: styleText(18, AppColors.primaryColor, false),
          ),
          SizedBox(
            height: 10,
          ),
          TextField(
            autofocus: false,
            style: TextStyle(fontSize: 22.0, color: Color(0xFFbdc6cf)),
            decoration: decorationTexfield(AppLocalizations
                .of(context)
                .email),
          ),
          SizedBox(
            height: 30,
          ),
          buttonSkip(AppLocalizations
              .of(context)
              .following, onTap: () {
            changeState(3);
          }),

        ],
      ),
    );
  }

  Widget tab3() {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Column(
        children: [

          Text(
            AppLocalizations
                .of(context)
                .createpassword,
            style: styleText(35, AppColors.primaryColor, false),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            AppLocalizations
                .of(context)
                .ingresspassword,
            style: styleText(18, AppColors.primaryColor, false),
          ),
          SizedBox(
            height: 10,
          ),
          TextField(
            autofocus: false,
            style: TextStyle(fontSize: 22.0, color: Color(0xFFbdc6cf)),
            decoration: decorationTexfield("**********"),
          ),
          Text(
            AppLocalizations
                .of(context)
                .confirmpassword,
            style: styleText(18, AppColors.primaryColor, false),
          ),
          SizedBox(
            height: 10,
          ),
          TextField(
            autofocus: false,
            style: TextStyle(fontSize: 22.0, color: Color(0xFFbdc6cf)),
            decoration: decorationTexfield("**********"),
          ),
          SizedBox(
            height: 20,
          ),
          buttonSkip(AppLocalizations
              .of(context)
              .following, onTap: () {
            changeState(4);
          }),

        ],
      ),
    );
  }

  Widget tab4() {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Column(
        children: [

          Text(
            AppLocalizations
                .of(context)
                .Whenyouwereborn,
            style: styleText(35, AppColors.primaryColor, false),
          ),
          SizedBox(
            height: 20,
          ),


          Row(
            children: [
              Expanded(
                flex: 2,
                child: Column(
                  children: [
                    Text(
                      AppLocalizations
                          .of(context)
                          .day,
                      style: styleText(18, AppColors.primaryColor, false),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    TextField(
                      autofocus: false,
                      style: TextStyle(
                          fontSize: 22.0, color: AppColors.primaryColor),
                      decoration: decorationTexfield(AppLocalizations
                          .of(context)
                          .day),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 10,),
              Expanded(
                flex: 2,
                child: Column(
                  children: [
                    Text(
                      AppLocalizations
                          .of(context)
                          .moth,
                      style: styleText(18, AppColors.primaryColor, false),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    TextField(
                      autofocus: false,
                      style: TextStyle(
                          fontSize: 22.0, color: AppColors.primaryColor),
                      decoration: decorationTexfield(AppLocalizations
                          .of(context)
                          .moth),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 10,),
              Expanded(
                flex: 2,
                child: Column(
                  children: [
                    Text(
                      AppLocalizations
                          .of(context)
                          .year,
                      style: styleText(18, AppColors.primaryColor, false),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    TextField(
                      autofocus: false,

                      style: TextStyle(
                          fontSize: 22.0, color: AppColors.primaryColor),
                      decoration: decorationTexfield(AppLocalizations
                          .of(context)
                          .year),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 60,
          ),


          buttonSkip(AppLocalizations
              .of(context)
              .following, onTap: () {
            changeState(5);
          }),
        ],
      ),
    );
  }

  Widget tab5() {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Column(
        children: [
          SizedBox(
            height: 40,
          ),
          Row(
            children: [
              Expanded(child: SizedBox()),
              SvgPicture.asset(AssetWidget().check),
              SizedBox(width: 20,),
              Text(
                "Registro \nCompleto",
                style: styleText(36, AppColors.primaryColor, false),
              ),
              Expanded(child: SizedBox()),
            ],
          ),

          SizedBox(
            height: 60,
          ),


          buttonSkip(AppLocalizations
              .of(context)
              .following, onTap: () {
            changeState(0);
          }),
        ],
      ),
    );
  }

  changeState(int state) {
    ReduxSignUp.store.dispatch(SetPostsStateActionSignUp(
      PostsStateSignUp(
          state: state),
    ));
    print(ReduxSignUp.store.state.postsState.state.toString());
  }
}
