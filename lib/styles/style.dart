import 'package:finzi/app/app_colors.dart';
import 'package:finzi/widgets/adapt_screen.dart';
import 'package:flutter/material.dart';

styleText(double size, var color, bool bold) {
  return TextStyle(
    fontSize: AdaptScreen.screenWidth() * 0.0 + size,
    color: color,
    fontWeight: bold != true ? FontWeight.normal : FontWeight.bold,
  );
}



decorationTextfield(String title) {
  return new InputDecoration(
    labelText: title,
    fillColor: Colors.white,
    hintStyle: TextStyle(fontSize: 17.0, color: Colors.white60),
    labelStyle: TextStyle(fontSize: 17.0, color: Colors.white60),
    enabledBorder: UnderlineInputBorder(
      borderSide: BorderSide(color: Colors.white60),
    ),
    focusedBorder: UnderlineInputBorder(
      borderSide: BorderSide(color: Colors.white),
    ),
    border: UnderlineInputBorder(
      borderSide: BorderSide(color: Colors.white),
    ),
  );
}

decorationTexfield(String title) {
  return InputDecoration(
    filled: true,
    fillColor: Color(0xffE8E9F5),
    hintText: title,
    hintStyle: styleText(23, AppColors.primaryColor, false),
    contentPadding: const EdgeInsets.only(left: 14.0, bottom: 8.0, top: 8.0),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Colors.white),
      borderRadius: BorderRadius.circular(10),
    ),
    enabledBorder: UnderlineInputBorder(
      borderSide: BorderSide(color: Colors.white),
      borderRadius: BorderRadius.circular(10),
    ),
  );
}

decorationContainerButton2() {
  return BoxDecoration(
    //  color: Colors.transparent,
    //gradient: LinearGradient(colors: [Color(0xFFC4C6DD), Color(0xFFE8E9FB)]),
    color: Color(0xffE3E4F2),
    borderRadius: BorderRadius.circular(30.0),
    boxShadow: [
      BoxShadow(
        color: Colors.grey.withOpacity(0.4),
        spreadRadius: 2,
        blurRadius: 3,
        offset: Offset(0, 3), // changes position of shadow
      ),
    ],
  );
}

decorationContainer() {
  return BoxDecoration(
    //  color: Colors.transparent,
    gradient: LinearGradient(colors: [Color(0xFFC4C6DD), Color(0xFFE8E9FB)]),
    borderRadius: BorderRadius.circular(30.0),
    boxShadow: [
      BoxShadow(
        color: Colors.grey.withOpacity(0.4),
        spreadRadius: 2,
        blurRadius: 3,
        offset: Offset(0, 3), // changes position of shadow
      ),
    ],
  );
}

decorationContainerButton() {
  return BoxDecoration(
    //  color: Colors.transparent,
    gradient: LinearGradient(colors: [Color(0xFF2BAD84), Color(0xFF1E8473)]),
    borderRadius: BorderRadius.circular(30.0),
  );
}

decorationTextfieldColorIcon(String title,Icon icon) {
  return new InputDecoration(
    labelText: title,
    fillColor: Colors.white,
    prefixIcon: icon,

    hintText: title,
    hintStyle: TextStyle(fontSize: 17.0, color: Colors.grey),
    labelStyle: TextStyle(fontSize: 17.0, color: Colors.grey),
    enabledBorder: UnderlineInputBorder(
      borderSide: BorderSide(color: Colors.grey),
    ),
    focusedBorder: UnderlineInputBorder(
      borderSide: BorderSide(color: Colors.grey),
    ),
    border: UnderlineInputBorder(
      borderSide: BorderSide(color: Colors.grey),
    ),
  );
}


