import 'dart:async';

import 'package:finzi/app/app_settings.dart';
import 'package:finzi/generated/l10n.dart';
import 'package:finzi/redux/sign_up/store.dart';
import 'package:finzi/ui/home.dart';
import 'package:finzi/widgets/adapt_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app_lock/flutter_app_lock.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'router/routers.dart';


bool init = false;

Future<void> main() async {
  await ReduxSignUp.init();
  runApp(AppLock(
    builder: (args) => MyApp2(),
    lockScreen: MyApp2(),
    enabled: false,
  ));
  init = true;
}

class MyApp2 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {



    AdaptScreen.initAdapt(context);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return MaterialApp(
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en', 'US'),
      ],
      builder: (context, child) {
        return MediaQuery(
          child: child,
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
        );
      },
      debugShowCheckedModeBanner: false,
      title: AppSettings.appDisplayName,
      theme: ThemeData(
        fontFamily: "Manrope",
        primarySwatch: Colors.blue,
      ),
      onGenerateRoute: RouteGenerator.generateRoute,
      home: Home(),
    );
  }
}


