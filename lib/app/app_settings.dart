class AppSettings {
  static const String appDisplayName = 'Finzi';
  static const String appSuffix = 'Finzi';
  static const bool debug = true;
  static const String version = '1.0.0';
  static const String build = '1';
  static const List<String> itemLanguage = ['English', 'Français'];
}
