import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryColor = Color(0xFF59205B);
  static const Color secondaryColor = const Color(0xFF2D9874);
  static const Color buttonColor = const Color(0xFFE8EAF7);

  static const Color backgroundColor = const Color(0xFFF2F2F7);
}
