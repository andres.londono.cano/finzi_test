import 'package:finzi/app/app_colors.dart';
import 'package:finzi/styles/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

String dropdownValue = 'One';
String dropdowLanguage = 'English';
bool checkSignUp = false;

List<String> spinnerItems = ['One'];

Widget button(String title, String subtitle, {Function onTap}) {
  return GestureDetector(
    onTap: (){
      onTap();

    },
    child: Container(
      //width: 140,
      height: 140,

      decoration: decorationContainerButton2(),

      child: Center(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            title,
            style: styleText(26, AppColors.primaryColor, false),
          ),
          Text(
            subtitle,
            style: styleText(28, AppColors.primaryColor, true),
          ),
        ],
      )),
    ),
  );
}

Widget buttonSkip(String title, {Function onTap}) {
  return Row(
    children: [
      Expanded(child: SizedBox()),
      GestureDetector(
        onTap: () {
          onTap();
        },
        child: Container(
          width: 165,
          height: 55,
          decoration: decorationContainerButton(),
          child: Center(
              child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                title,
                style: styleText(22, Colors.white, false),
              ),
              SizedBox(
                width: 10,
              ),
              Icon(
                Icons.arrow_forward,
                color: Colors.white,
              ),
            ],
          )),
        ),
      ),
    ],
  );
}

Widget buttonHome(String url, {Function onTap}) {
  return GestureDetector(
    onTap: () {
      onTap();
    },
    child: Container(
        width: 65,
        height: 65,
        padding: EdgeInsets.all(13),
        child:
            Container(padding: EdgeInsets.all(5), child: SvgPicture.asset(url)),
        decoration: decorationContainer()),
  );
}
