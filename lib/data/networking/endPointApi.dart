
import 'package:finzi/app/app_settings.dart';
import 'package:finzi/redux/app/app_state.dart';

import 'package:redux/redux.dart';


class endPointApi {
  endPointApi();

  Store<AppState> store;

  Future<void> loadStore(Store<AppState> store) async {
    this.store = store;
  }

  static const bool debug = AppSettings.debug;
  static const String version = AppSettings.version;
  static const String mnt = '';
  static const String baseUrl = AppSettings.debug ? '' : '';

}

class MyHttpResponse {
  int statusCode;
  String message;
  dynamic data;

  MyHttpResponse(this.statusCode, this.data, {this.message});

  @override
  String toString() {
    return 'MyHttpResponse{statusCode: $statusCode, message: $message, data: $data}';
  }
}
