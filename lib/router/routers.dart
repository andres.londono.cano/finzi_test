import 'package:flutter/material.dart';


const String homeRoute = '/';
const String profileRoute = 'profile';
const String homeRoutes = "home";
const String homeRoutes2 = "home2";
const String loginRoute = "login";

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case homeRoutes:

      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(resizeToAvoidBottomInset: true,
                  body: Center(
                      child: Text('No route defined for ${settings.name}')),
                ));
    }
  }
}
