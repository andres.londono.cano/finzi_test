// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class AppLocalizations {
  AppLocalizations();
  
  static AppLocalizations current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<AppLocalizations> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      AppLocalizations.current = AppLocalizations();
      
      return AppLocalizations.current;
    });
  } 

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  /// `¿Quién eres?`
  String get whoareyou {
    return Intl.message(
      '¿Quién eres?',
      name: 'whoareyou',
      desc: '',
      args: [],
    );
  }

  /// `Soy un`
  String get iama {
    return Intl.message(
      'Soy un',
      name: 'iama',
      desc: '',
      args: [],
    );
  }

  /// `Joven`
  String get young {
    return Intl.message(
      'Joven',
      name: 'young',
      desc: '',
      args: [],
    );
  }

  /// `Padre`
  String get dad {
    return Intl.message(
      'Padre',
      name: 'dad',
      desc: '',
      args: [],
    );
  }

  /// `¿Cómo te llamas?`
  String get whatisyourname {
    return Intl.message(
      '¿Cómo te llamas?',
      name: 'whatisyourname',
      desc: '',
      args: [],
    );
  }

  /// `Ingresa tu nombre`
  String get ingressname {
    return Intl.message(
      'Ingresa tu nombre',
      name: 'ingressname',
      desc: '',
      args: [],
    );
  }

  /// `Nombre`
  String get name {
    return Intl.message(
      'Nombre',
      name: 'name',
      desc: '',
      args: [],
    );
  }

  /// `Apellido`
  String get apellido {
    return Intl.message(
      'Apellido',
      name: 'apellido',
      desc: '',
      args: [],
    );
  }

  /// `Ingresa tu apellido`
  String get ingresslastname {
    return Intl.message(
      'Ingresa tu apellido',
      name: 'ingresslastname',
      desc: '',
      args: [],
    );
  }

  /// `Siguiente`
  String get following {
    return Intl.message(
      'Siguiente',
      name: 'following',
      desc: '',
      args: [],
    );
  }

  /// `¿Cuál es tu email?`
  String get whatisyouremail {
    return Intl.message(
      '¿Cuál es tu email?',
      name: 'whatisyouremail',
      desc: '',
      args: [],
    );
  }

  /// `Ingresa tu email`
  String get ingressemail {
    return Intl.message(
      'Ingresa tu email',
      name: 'ingressemail',
      desc: '',
      args: [],
    );
  }

  /// `Email`
  String get email {
    return Intl.message(
      'Email',
      name: 'email',
      desc: '',
      args: [],
    );
  }

  /// `Crea tu contraseña`
  String get createpassword {
    return Intl.message(
      'Crea tu contraseña',
      name: 'createpassword',
      desc: '',
      args: [],
    );
  }

  /// `Ingresa tu contraseña`
  String get ingresspassword {
    return Intl.message(
      'Ingresa tu contraseña',
      name: 'ingresspassword',
      desc: '',
      args: [],
    );
  }

  /// `Confirmar contraseña`
  String get confirmpassword {
    return Intl.message(
      'Confirmar contraseña',
      name: 'confirmpassword',
      desc: '',
      args: [],
    );
  }

  /// `¿Cuándo naciste?`
  String get Whenyouwereborn {
    return Intl.message(
      '¿Cuándo naciste?',
      name: 'Whenyouwereborn',
      desc: '',
      args: [],
    );
  }

  /// `Día`
  String get day {
    return Intl.message(
      'Día',
      name: 'day',
      desc: '',
      args: [],
    );
  }

  /// `Mes`
  String get moth {
    return Intl.message(
      'Mes',
      name: 'moth',
      desc: '',
      args: [],
    );
  }

  /// `Year`
  String get year {
    return Intl.message(
      'Year',
      name: 'year',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'es'),
      Locale.fromSubtags(languageCode: 'fr'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<AppLocalizations> load(Locale locale) => AppLocalizations.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}