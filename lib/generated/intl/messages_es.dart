// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "Whenyouwereborn" : MessageLookupByLibrary.simpleMessage("¿Cuándo naciste?"),
    "apellido" : MessageLookupByLibrary.simpleMessage("Apellido"),
    "confirmpassword" : MessageLookupByLibrary.simpleMessage("Confirmar contraseña"),
    "createpassword" : MessageLookupByLibrary.simpleMessage("Crea tu contraseña"),
    "dad" : MessageLookupByLibrary.simpleMessage("Padre"),
    "day" : MessageLookupByLibrary.simpleMessage("Día"),
    "email" : MessageLookupByLibrary.simpleMessage("Email"),
    "following" : MessageLookupByLibrary.simpleMessage("Siguiente"),
    "iama" : MessageLookupByLibrary.simpleMessage("Soy un"),
    "ingressemail" : MessageLookupByLibrary.simpleMessage("Ingresa tu email"),
    "ingresslastname" : MessageLookupByLibrary.simpleMessage("Ingresa tu apellido"),
    "ingressname" : MessageLookupByLibrary.simpleMessage("Ingresa tu nombre"),
    "ingresspassword" : MessageLookupByLibrary.simpleMessage("Ingresa tu contraseña"),
    "moth" : MessageLookupByLibrary.simpleMessage("Mes"),
    "name" : MessageLookupByLibrary.simpleMessage("Nombre"),
    "whatisyouremail" : MessageLookupByLibrary.simpleMessage("¿Cuál es tu email?"),
    "whatisyourname" : MessageLookupByLibrary.simpleMessage("¿Cómo te llamas?"),
    "whoareyou" : MessageLookupByLibrary.simpleMessage("¿Quién eres?"),
    "year" : MessageLookupByLibrary.simpleMessage("Year"),
    "young" : MessageLookupByLibrary.simpleMessage("Joven")
  };
}
